# Conductivity 2 Resistance Calculator
## by: Timothy Carter (aka: SmilingTexan)

This is a project I worked on some time ago. It was created when I was working with
[Advantage Controls](http://www.advantagecontrols.com).

This will calculate specific resistances needed to simulate a certain Conductivity value
on their controllers. Similar to the pump output calculator, that I've also posted here,
this one was accurate as of 2013 and may no longer be as I no longer work with them.

The fortunate part is that the code has been made generic and will allow you to
enter electrode diameter and distance and calculate the "cell constant" of the electrode.
Which means *theoretically* at least that you could use any manufacturers controller.
