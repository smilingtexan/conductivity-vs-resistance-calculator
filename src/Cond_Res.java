/**
 *	file: Cond_Res.java
 *
 *	author: Timothy Carter
 *
 *	date: October 21, 2009
 *
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Cond_Res extends JPanel implements ActionListener {
	private static final long serialVersionUID = -3567606090166964483L;
	static Color Blue = new Color(0, 0, 200);
	static Color White = new Color(250, 250, 250);
	static Color Red = new Color(150, 0, 0);
	static Font font = new Font("Verdana", Font.BOLD, 12);
	static Font ResultFont = new Font("Verdana", Font.ITALIC, 18);
	static JLabel CellConstantInfo = new JLabel();
	static JTextField Distance = new JTextField();
	static JLabel DistanceUnits = new JLabel();
	static JLabel DiameterInfo = new JLabel();
	static JTextField Diameter = new JTextField();
	static JLabel DistanceUnits2 = new JLabel();
	static JLabel CellConstant = new JLabel();
	static JLabel SenseArea = new JLabel();
	static JLabel Area = new JLabel();
	static JLabel AreaUnits = new JLabel();
	static JTextField Cell = new JTextField();
	static JLabel CellConstantUnits = new JLabel();
	static JLabel Conductivity = new JLabel();
	static JTextField Cond = new JTextField();
	static JLabel CondUnits = new JLabel();
	static JLabel Conductance = new JLabel();
	static JLabel Conduct = new JLabel();
	static JLabel ConductUnits = new JLabel();
	static JLabel Resistance = new JLabel();
	static JTextField Resist = new JTextField();
	static JLabel ResUnits = new JLabel();
	static JLabel Disclaimer = new JLabel();
	static JLabel HighCond = new JLabel();
	static JTextField HighCondInput = new JTextField();
	
	public Cond_Res() {
		JPanel DistancePanel = new JPanel();
		DistancePanel.setLayout(new BoxLayout(DistancePanel, BoxLayout.X_AXIS));
		CellConstantInfo.setText("Distance between the drive & sense electrodes: ");
		CellConstantInfo.setForeground(White);
		Distance.setColumns(5);
		Distance.setText("0");
		Distance.addActionListener(this);
		Distance.setActionCommand("Distance_Change");
		DistanceUnits.setText(" cm  ");
		DistanceUnits.setForeground(White);
		DistancePanel.setOpaque(false);
		DistancePanel.add(CellConstantInfo);
		DistancePanel.add(Distance);
		DistancePanel.add(DistanceUnits);
		
		JPanel AreaPanel = new JPanel();
		AreaPanel.setLayout(new BoxLayout(AreaPanel, BoxLayout.X_AXIS));
		AreaPanel.setOpaque(false);
		DiameterInfo.setText("  Diameter of the sense electrode: ");
		DiameterInfo.setForeground(White);
		Diameter.setColumns(5);
		Diameter.setText("0");
		Diameter.addActionListener(this);
		Diameter.setActionCommand("Diameter_Change");
		DistanceUnits2.setText(" cm  ");
		DistanceUnits2.setForeground(White);
		AreaPanel.add(DiameterInfo);
		AreaPanel.add(Diameter);
		AreaPanel.add(DistanceUnits2);
		
		JPanel CellConstantPanel = new JPanel();
		CellConstantPanel.setLayout(new BoxLayout(CellConstantPanel, BoxLayout.X_AXIS));
		CellConstantPanel.setOpaque(false);
		SenseArea.setForeground(Red);
		SenseArea.setText(" Sense Electrode Area: ");
		SenseArea.setFont(font);
		Area.setText("0");
		Area.setForeground(Red);
		Area.setFont(font);
		AreaUnits.setText(" cm�");
		AreaUnits.setForeground(Red);
		AreaUnits.setFont(font);
		CellConstant.setForeground(Red);
		CellConstant.setText("    Cell Constant (k):   ");
		CellConstant.setFont(font);
		Cell.setText("0");
		Cell.setEnabled(false);
		Cell.setDisabledTextColor(Blue);
		Cell.setColumns(8);
		Cell.setFont(font);
		CellConstantUnits.setText(" /cm");
		CellConstantUnits.setForeground(White);
		CellConstantUnits.setFont(font);
		CellConstantPanel.add(SenseArea);
		CellConstantPanel.add(Area);
		CellConstantPanel.add(AreaUnits);
		CellConstantPanel.add(CellConstant);
		CellConstantPanel.add(Cell);
		CellConstantPanel.add(CellConstantUnits);
		
		JPanel ConductivityPanel = new JPanel();
		ConductivityPanel.setLayout(new BoxLayout(ConductivityPanel, BoxLayout.X_AXIS));
		ConductivityPanel.setOpaque(false);
		Conductivity.setText("Conductivity: ");
		Conductivity.setForeground(White);
		Conductivity.setFont(ResultFont);
		Cond.setText("0");
		Cond.setColumns(8);
		Cond.addActionListener(this);
		Cond.setActionCommand("Cond_Change");
		Cond.setFont(ResultFont);
		CondUnits.setText(" �S/cm          ");
		CondUnits.setForeground(White);
		CondUnits.setFont(ResultFont);
		ConductivityPanel.add(Conductivity);
		ConductivityPanel.add(Cond);
		ConductivityPanel.add(CondUnits);
		
		JPanel ConductancePanel = new JPanel();
		ConductancePanel.setLayout(new BoxLayout(ConductancePanel, BoxLayout.X_AXIS));
		ConductancePanel.setOpaque(false);
		Conductance.setText(" Conductance: ");
		Conductance.setForeground(Red);
		Conductance.setFont(ResultFont);
		Conduct.setText("0");
		Conduct.setForeground(Red);
		Conduct.setFont(ResultFont);
		ConductUnits.setText(" �S          ");
		ConductUnits.setForeground(Red);
		ConductUnits.setFont(ResultFont);
		ConductancePanel.add(Conductance);
		ConductancePanel.add(Conduct);
		ConductancePanel.add(ConductUnits);
		
		JPanel ResistancePanel = new JPanel();
		ResistancePanel.setLayout(new BoxLayout(ResistancePanel, BoxLayout.X_AXIS));
		ResistancePanel.setOpaque(false);
		Resistance.setText("Resistance: ");
		Resistance.setForeground(White);
		Resistance.setFont(ResultFont);
		Resist.setText("0");
		Resist.setColumns(8);
		Resist.addActionListener(this);
		Resist.setActionCommand("Resist_Change");
		Resist.setFont(ResultFont);
		ResUnits.setText(" ohms");
		ResUnits.setForeground(White);
		ResUnits.setFont(ResultFont);
		ResistancePanel.add(Resistance);
		ResistancePanel.add(Resist);
		ResistancePanel.add(ResUnits);
		
		JPanel DisclaimerPanel = new JPanel();
		DisclaimerPanel.setLayout(new BoxLayout(DisclaimerPanel, BoxLayout.X_AXIS));
		DisclaimerPanel.setOpaque(false);
		Disclaimer.setText("Conductivity / Resistance values are at 77 degrees F");
		Disclaimer.setForeground(Blue);
		Disclaimer.setFont(font);
		DisclaimerPanel.add(Disclaimer);
		
		JPanel HighCondPanel = new JPanel();
		HighCondPanel.setLayout(new BoxLayout(HighCondPanel, BoxLayout.X_AXIS));
		HighCondPanel.setOpaque(false);
		HighCond.setText("Conductivity Reading @ 300 degrees F  ");
		HighCond.setForeground(White);
		HighCondInput.setText("0");
		HighCondInput.setColumns(8);
		HighCondInput.setFont(getFont().deriveFont(Font.BOLD));
		HighCondPanel.add(HighCond);
		HighCondPanel.add(HighCondInput);
		
		add(DistancePanel, BorderLayout.PAGE_START);
		add(AreaPanel);
		add(CellConstantPanel);
		add(ConductivityPanel);
		add(ConductancePanel);
		add(ResistancePanel);
		add(DisclaimerPanel);
		add(HighCondPanel);
		setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
	}
	
	private void calculate_constant() {
		float temp = 0, temp1 = 0;
		temp = Float.parseFloat(Distance.getText());		// retrieve distance
		temp1 = Float.parseFloat(Diameter.getText());		// retrieve diameter
		temp1 /= 2;		// get radius
		temp1 *= temp1;		// radius squared
		temp1 *= 3.14159;	// get area - temp1 should now contain the area of the sense electrode
		Area.setText(String.valueOf(temp1));
		if (temp1 > 0) {
			temp /= temp1;
			Cell.setText(String.valueOf(temp));
		} else {
			Cell.setText("Div by 0");
		}
	}
	
	private void calculate_cond() {
		int temp = 0;
		temp = Integer.parseInt(Cond.getText());		// get Conductivity (uS/cm)
		if (Cell.getText().equals("Div by 0")) {
			temp *= 0;				// Make sure we have a number
		} else {
			temp *= Float.parseFloat(Cell.getText());	// Conductivity in uS/sq.cm
		}
		temp *= Float.parseFloat(Area.getText());		// Conductivity in uS
		Conduct.setText(String.valueOf(temp));
	}
	
	private void calculate_resist() {
		float temp = 0;
		int temp1 = 0;
		temp = Float.parseFloat(Conduct.getText());		// get conductance
		temp /= 1000000;			// put into S (make sure we have x10^-6)
		temp1 = (int)(1 / temp);			// take recipricol
		if (Conduct.getText().equals("0")) {
			Resist.setText("0");
		} else {
			Resist.setText(String.valueOf(temp1));
		}
	}
	
	private void calculate_conductive() {
		float temp = 0;
		temp = (1 / Float.parseFloat(Resist.getText()));
		temp *= 1000000;
		Conduct.setText(String.valueOf((int)temp));
		temp /= Float.parseFloat(Area.getText());
		temp /= Float.parseFloat(Cell.getText());
		Cond.setText(String.valueOf((int)temp));
	}

	private void calculate_highcond() {
		float temp = 0;
		temp = Float.parseFloat(Cond.getText());	// raw reading = cond. @ 77
		temp *= 1000;
		temp /= (1000 + 11 * (300 - 77));
		HighCondInput.setText(String.valueOf((int)temp));
	}
	
	public void actionPerformed(ActionEvent e) {
		if ("Distance_Change".equals(e.getActionCommand())) {
			calculate_constant();
			calculate_cond();
			calculate_resist();
			calculate_highcond();
			Diameter.grabFocus();
		}
		if ("Diameter_Change".equals(e.getActionCommand())) {
			calculate_constant();
			calculate_cond();
			calculate_resist();
			calculate_highcond();
			Cond.grabFocus();
		}
		if ("Cond_Change".equals(e.getActionCommand())) {
			calculate_constant();		
			calculate_cond();			
			calculate_resist();
			calculate_highcond();
			Resist.grabFocus();
		}
		if ("Resist_Change".equals(e.getActionCommand())) {
			calculate_constant();
			calculate_conductive();
			calculate_highcond();
			Cond.grabFocus();
		}
	}

	/**
	 * Setup the constraints so that the user supplied component and the background image
	 * label overlap and resize identically
	 * 
	 */
	private static final GridBagConstraints gbc;
	static {
		gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.weightx = 1.0;
		gbc.weighty = 1.0;
		gbc.fill = GridBagConstraints.BOTH;
		gbc.anchor = GridBagConstraints.NORTHWEST;
	}

	/**
	 * Wraps a Swing component in a background image
	 * Simply invokes the overloaded variant with Top/Leading alignment for background image
	 * @param component - to wrap the background image
	 * @param backgroundIcon - the background image (Icon)
	 * @return the wrapping JPanel 
	 */
	public static JPanel wrapInBackgroundImage(JComponent component, Icon backgroundIcon) {
		return wrapInBackgroundImage(component, backgroundIcon, JLabel.TOP, JLabel.LEADING);
	}
	
	/**
	 * Wraps a Swing component in a background image
	 * The vertical and horizontal alignment constrains the JLabel
	 * @param component - to wrap the background image
	 * @param backgroundIcon - the background image (Icon)
	 * @param verticalAlignment - vertical Alignment
	 * @param horizontalAlignment - horizontal Alignment
	 * @return the wrapping JPanel 
	 */
	public static JPanel wrapInBackgroundImage(JComponent component, Icon backgroundIcon, int verticalAlignment, int horizontalAlignment) {
		// make the passed in swing component transparent
		component.setOpaque(false);
		
		// create wrapper JPanel
		JPanel backgroundPanel = new JPanel(new GridBagLayout());
		
		// add the passed in swing component first to ensure it is in front
		backgroundPanel.add(component, gbc);
		
		// create a label to paint the background image
		JLabel backgroundImage = new JLabel(backgroundIcon);
		
		// set minimum and preferred sized so that the size of the image does no affect the layout size
		backgroundImage.setPreferredSize(new Dimension(1,1));
		backgroundImage.setMinimumSize(new Dimension(1,1));
		
		// align the image as specified
		backgroundImage.setVerticalAlignment(verticalAlignment);
		backgroundImage.setHorizontalAlignment(horizontalAlignment);
		
		// add the background label
		backgroundPanel.add(backgroundImage, gbc);
		
		// return the wrapper
		return backgroundPanel;
	}

	/**
	 * Returns an ImageIcon, or null if the path was invalid
	 *  
	 */
	protected static ImageIcon createImageIcon(String path) {
		java.net.URL imgURL = Cond_Res.class.getResource(path);
		if (imgURL != null) {
			return new ImageIcon(imgURL);
		} else {
			System.err.println("Couldn't find file: " + path);
			return null;
		}
	}

	public static void createAndShowGUI() {
		// get screen size & middle of screen position
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int windowX = Math.max(0, (screenSize.width - 600) / 2);
		int windowY = Math.max(0, (screenSize.height - 275) / 2);
		
		// Create and setup the window
		JFrame.setDefaultLookAndFeelDecorated(false);
		JFrame frame = new JFrame("Conductivity - Resistance Calculator");
		
		// Create and setup the content pane
		JComponent foregroundPanel = new Cond_Res();
		foregroundPanel.setOpaque(true);

		frame.setContentPane(wrapInBackgroundImage(foregroundPanel, (createImageIcon("images/bg.jpg"))));
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) { System.exit(0); }
		});
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// Display the window
		java.net.URL imgURL = Cond_Res.class.getResource("images/Adv-Icon.gif");
		frame.pack();
		frame.setIconImage(new ImageIcon(imgURL).getImage());
		frame.setBackground(Blue);
		frame.setResizable(false);
		frame.setSize(600, 275);
		frame.setLocation(windowX, windowY);
		frame.setVisible(true);
		
	}

	public static void main (String[] args) {
		// Schedule a job for the event-dispatching thread:
		// creating and showing this application's GUI
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}